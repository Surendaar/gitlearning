package com.level2;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class flighTakeOff {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc= new Scanner(System.in);
		int tc = sc.nextInt();
		for(int i=0;i<tc; i++) {
			int numOfDays= sc.nextInt();
			int flightA = sc.nextInt();
			int flightB = sc.nextInt();
			int flightC = sc.nextInt();
			findSuccessDays(numOfDays,flightA, flightB, flightC );
			//display(successDays);
			//System.out.println();
			//display(failureDays);
		}
	}
	static Set<Integer> failureDays, successDays;
	private static void findSuccessDays(int numOfDays, int flightA, int flightB, int flightC) {
		// TODO Auto-generated method stub
		failureDays= new TreeSet<Integer>();
		successDays= new TreeSet<Integer>();
		boolean fail=false, pass= false;
		int successdaycount=0;
		for(int i=flightA; i<=numOfDays; i=i+flightA) {
			fail=false; pass= false;
			fail=isAvailableInFailureDays(i);
			if(fail==false) {
			pass=isAvailableInSucessDays(i);
			if(pass==false)
				successdaycount++;
			else
				successdaycount--;
			}
		}
		for(int i=flightB; i<=numOfDays; i=i+flightB) {
			fail=false; pass= false;
			fail=isAvailableInFailureDays(i);
			if(fail==false) {
			pass=isAvailableInSucessDays(i);
			if(pass==false)
				successdaycount++;
			else
				successdaycount--;
			}
		}
		for(int i=flightC; i<=numOfDays; i=i+flightC) {
			fail=false; pass= false;
			fail=isAvailableInFailureDays(i);
			if(fail==false) {
			pass=isAvailableInSucessDays(i);
			if(pass==false)
				successdaycount++;
			else
				successdaycount--;
			}
		}
		System.out.println(successdaycount);
		
	}
	private static void display(Set<Integer> successDays2) {
		// TODO Auto-generated method stub
		System.out.println("*******");
		for(Integer i:successDays2)
			System.out.print(i+" ");
	}
	private static boolean isAvailableInSucessDays(int num) {
		boolean check=false;
		for(Integer i:successDays)
			if(i==num) {
				check=true;
				failureDays.add(i);
			}
		if(check==true) {
			successDays.remove(num);
			return true;
		}
		else {
			successDays.add(num);
			return false;
		}
	}
	private static boolean isAvailableInFailureDays(int num) {
		// TODO Auto-generated method stub
		boolean check=false;
		for(Integer i:failureDays)
			if(i==num) 
				check=true;
		if(check==true)
			return true;
		else
		return false;
	}

}
