package com.level2;

import java.util.Scanner;

public class symmetricLogo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int tc=sc.nextInt();
		for(int a=0; a<tc; a++) {
			int size=sc.nextInt();
			String[] str = new String[size];
			for(int i=0; i<size; i++)
				str[i] = sc.next();
			int[][] mat = new int[size][size];
			for(int i=0; i<size; i++)
				for(int j=0; j<size; j++)
					mat[i][j]=str[i].charAt(j)-48;
			display(mat, size);
			findSymmetric(size, mat);
		}
	}

	private static void display(int[][] mat, int size) {
		// TODO Auto-generated method stub
		for(int i=0; i<size; i++) {
			System.out.println();
			for(int j=0; j<size; j++)
				System.out.print(mat[i][j]+" ");
		}
	}

	private static void findSymmetric(int size, int[][] mat) {
		// TODO Auto-generated method stub
		boolean check=true;
		for(int i=0; i<size; i++)
			for(int j=0; j<size && check==true; j++) {
				if(mat[i][j]==1) {
					if(mat[size-i-1][j]!=1)
						check=false;
					if(mat[i][size-j-1]!=1)
						check=false;
				}
			}
		if(check==true)
			System.out.println("YES");
		else
			System.out.println("NO");
	}

}
