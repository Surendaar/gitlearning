package com.level2.string;

import java.util.Scanner;

public class clothDrying {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc= new Scanner(System.in);
		int numOfRain =sc.nextInt();
		int numOfCloths = sc.nextInt();
		int numOfTimes = sc.nextInt();
		int result=0;
		int timeOfRains[] = new int[numOfRain];
		for(int i=0; i<numOfRain; i++)
			timeOfRains[i]=sc.nextInt();
		
			int clothDriesIn[] = new int[numOfCloths];
		for(int i=0; i<numOfCloths; i++)
			clothDriesIn[i]=sc.nextInt();
		
		for(int j=0; j<numOfTimes; j++) {
			int diff=timeOfRains[j+1]-timeOfRains[j];
			for(int i=0; i<numOfCloths; i++)
				{
				int res=clothDriesIn[i]-diff;
				if(res<=0) {
					clothDriesIn[i]=999;
					result++;
				}
				}
		}
		System.out.println(result);
	}
}
