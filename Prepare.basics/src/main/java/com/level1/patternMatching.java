package com.level1;

import java.util.Scanner;

public class patternMatching {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int tc= sc.nextInt();
		while(tc>0) {
			tc--;
			int size=sc.nextInt();
			String[] mat = new String[size];
			for(int i=0; i<size; i++)
					mat[i]=sc.next();
			findMatch(size, mat);
		}
	}

	private static void findMatch(int size, String[] mat) {
		// TODO Auto-generated method stub
		boolean hori=true, vert=true;
		for(int i=0; i<size/2; i++) {
			for(int j=0; j<size/2; j++)
			if(mat[i].charAt(j)!=mat[i].charAt(size-j))
				hori=false;
			for(int j=0; j<size; j++)
				if(mat[i].charAt(j)!=mat[size-i].charAt(j))
					vert=false;
		}
		if(hori==vert==true)
			System.out.println("BOTH");
		else if(hori==true)
			System.out.println("HORIZONTAL");
		else if(vert==true)
			System.out.println("VERTICAL");
		else
			System.out.println("NO");
	}

}
