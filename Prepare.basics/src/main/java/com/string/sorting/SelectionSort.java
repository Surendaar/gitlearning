package com.string.sorting;

import java.util.Scanner;

public class SelectionSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int size = sc.nextInt();
		int iteration = sc.nextInt();
		int arr[] = new int[size];
		for(int i=0; i<size; i++)
			arr[i]=sc.nextInt();
		sort(arr, size, iteration);
		
	}

	private static void sort(int[] arr, int size, int iteration) {
		// TODO Auto-generated method stub
		int max=0;
		for(int i=0; i<iteration && i<size; i++)
			for(int j=1; j<size-i; j++) {
				if(arr[j]>arr[max])
					max=j;
				if(j==size-1-i)
					if(arr[max]<arr[j])
					{
						
						int temp = arr[j];
						arr[j]=arr[max];
						System.out.println(max+ " "+temp +" "+ arr[max]);
						arr[max]=temp;
					}
			}
		
		display(arr, size);
	}

	private static void display(int[] arr, int size) {
		// TODO Auto-generated method stub
		for(int i=0; i<size; i++)
			System.out.print(arr[i]+" ");
	}

}
