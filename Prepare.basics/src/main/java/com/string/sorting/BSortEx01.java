package com.string.sorting;

import java.util.Scanner;

public class BSortEx01 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int tc = sc.nextInt();
		for(int i=0; i<tc; i++) {
			int size=sc.nextInt();
			int[] arr = new int[size];
			int min = sc.nextInt();
			for(int j=0; j<size; j++) 
				arr[j]=sc.nextInt();
			arr=bubbleSort(arr, size);
			difference(arr, size, size-min);
		}
	}
	
	private static void difference(int[] arr, int size, int range) {
		// TODO Auto-generated method stub
		int min=0, max=0;
		for(int i=0; i<range; i++)
			min=min+arr[i];
		for(int i=size-range; i<size; i++)
			max=max+arr[i];
		System.out.println(max-min);
	}

	private static int[] bubbleSort(int[] arr, int size) {
		// TODO Auto-generated method stub
		//int count=0;
		for(int j=0; j< size-1; j++) {
			for(int i=0; i<size-j-1; i++) {
				if(arr[i]>arr[i+1]) {
					int temp = arr[i];
					arr[i] = arr[i+1];
					arr[i+1]=temp;
					//System.out.println(temp+ " "+ arr[i]);
					//count++;
				}
			}
		}
		return arr;
	}

}
