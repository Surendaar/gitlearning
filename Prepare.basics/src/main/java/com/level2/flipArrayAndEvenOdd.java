package com.level2;

import java.util.Scanner;

public class flipArrayAndEvenOdd {
	static byte[] arr;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int arraySize = sc.nextInt();
		int numOfQueries = sc.nextInt();
		arr = new byte[arraySize];
		for(int i=0; i<arraySize; i++) {
			arr[i]=sc.nextByte();
		}
		for(int i=0; i<numOfQueries; i++) {
			int flipOrEven= sc.nextInt();
			if(flipOrEven==0) {
				int startingIndex = sc.nextInt();
				int endingIndex = sc.nextInt();
				evenOrOdd(startingIndex, endingIndex);
			}
			if(flipOrEven==1) {
				int flipIndex=sc.nextInt();
				if(arr[flipIndex-1]==0)
					arr[flipIndex-1]=1;
				else
					arr[flipIndex-1]=0;
			}
		}

	}
	private static void evenOrOdd( int startingIndex, int endingIndex) {
		// TODO Auto-generated method stub
		if(arr[endingIndex-1]==0)
			System.out.println("EVEN");
		else
			System.out.println("ODD");
	}

}
