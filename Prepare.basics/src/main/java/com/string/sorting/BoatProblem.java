package com.string.sorting;

import java.util.Scanner;
import java.util.TreeSet;

public class BoatProblem {
	static TreeSet<Integer> one= new TreeSet<Integer>();
	static TreeSet<Integer> two= new TreeSet<Integer>();
	static int minweight=0;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc= new Scanner(System.in);
		int tc = sc.nextInt();
		for(int j=0; j<tc; j++) {
			int size=sc.nextInt();
			
			for(int i=0; i<size; i++)
				one.add(sc.nextInt());
			//display(one);
			//display(two);
			findShortestBoatRide();
			System.out.println(minweight);
			minweight=0;
			one= new TreeSet<Integer>();
			two= new TreeSet<Integer>();
		}
	}

	private static void findShortestBoatRide() {
		// TODO Auto-generated method stub
		boolean check=false, exit=false;
		while(exit==false) {
			if(check==false) {
				exit=sendTwoMinimumWtToOtherShore();
				if(exit==true)
					break;
				sendOneMinimumwtToShoreA();
				check=true;
				//System.out.println("++"+minweight);
				//display(one);
				//display(two);
			}
			else
			{
				exit=sendTwoMaximumWtToOtherShore();
				if(exit==true)
					break;
				sendOneMinimumwtToShoreA();
				check=false;
				//System.out.println("+++"+minweight);
				//display(one);
				//display(two);
			}
		}
	}

	private static void display(TreeSet<Integer> one2) {
		// TODO Auto-generated method stub
		System.out.println();
		for(Integer a:one2)
			System.out.print(a+" ");
	}

	private static boolean sendTwoMaximumWtToOtherShore() {
		// TODO Auto-generated method stub
		int last, lastTwo;
		int size=one.size();
		
		last=one.last();
		one.remove(last);
		two.add(last);
		
		if(size>1) {
		lastTwo=one.last();
		one.remove(lastTwo);
		two.add(lastTwo);
		}
		else
			lastTwo=0;
		minweight=minweight+last;
		if(size<=2)
			return true;
		else
			return false;
	}

	private static void sendOneMinimumwtToShoreA() {
		// TODO Auto-generated method stub
		int first;
		first=two.first();
		one.add(first);
		two.remove(first);
		minweight=minweight+first;
	}

	private static boolean sendTwoMinimumWtToOtherShore() {
		// TODO Auto-generated method stub
		int first, second;
		int size=one.size();
		
		first=one.first();
		one.remove(first);		
		two.add(first);
		if(size>1)
		{
		second=one.first();
		one.remove(second);
		two.add(second);
		minweight=minweight+second;
		}
		else {
			second=0;
			minweight=minweight+first;
		}
		if(size<=2)
			return true;
		else
			return false;
	}

}
