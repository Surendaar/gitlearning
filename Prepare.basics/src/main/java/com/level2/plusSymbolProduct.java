package com.level2;

import java.util.Scanner;

public class plusSymbolProduct {
public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	int x=sc.nextInt();
	int y=sc.nextInt();
	char mat[][]= new char[x][y];
	String str[] = new String[x];
	for(int i=0; i<x; i++)
		str[i]=sc.next();
	for(int i=0; i<x; i++)
		for(int j=0; j<y; j++)
			mat[i][j]=str[i].charAt(j);
	//display(mat, x, y);
	findPlus(mat, x, y);
}

private static void findPlus(char[][] mat, int x, int y) {
	// TODO Auto-generated method stub
	int pluses[] = new int[x];
	int plus=0;
	for(int i=0; i<x; i++)
		for(int j=0; j<y; j++) {
			if(mat[i][j]=='R')
			{
				int count=countplus(mat, i, j, x, y);
				if(count>0)
				{
					pluses[plus]=count;
					plus++;
				}
			}
		}
	display(pluses, plus);
}

private static void display(int[] pluses, int plus) {
	// TODO Auto-generated method stub
	System.out.println("+++++");
	for(int i=0; i<plus; i++)
		System.out.println(pluses[i]+" ");
}

private static int countplus(char[][] mat, int i, int j, int x, int y) {
	// TODO Auto-generated method stub
	int col=0, row=0;
	if(mat[i][j]=='R')
	{
		System.out.println(i+" "+j);
		for(int m=i; mat[m][j]=='R' && m<x; m++)
			row++;
		if(row>2 && row%2==1) {
			for(int n=j-(row/2)+1; mat[(row/2)+1][n]=='R' && n<y && n<j+(row/2)+1 ; n++)
				col++;
		}
		if(row>2 && row==col)
			return row+col-1;
	}
	return 0;
}

private static void display(char[][] mat, int x, int y) {
	// TODO Auto-generated method stub
	for(int i=0; i<x; i++) {
		for(int j=0; j<y; j++)
			System.out.print(mat[i][j]+" ");
		System.out.println();
	}
	
}
}
