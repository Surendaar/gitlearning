package copiedCode;

import java.util.Scanner;

class TestClass {
    public static void main(String args[] ) throws Exception {
        Scanner sc=new Scanner(System.in);
        int size=sc.nextInt();
        int board[][]=new int[size][size];
        if(check(board,0,size)) {
        	System.out.println("YES");
        	display(board,size);
        }
        else 
        	System.out.println("NO");
    }
    public static boolean check(int board[][],int row,int size){
        if(row==size) 
        	return true;
        for(int col=0;col<size;col++){
            if(place(board,row,col,size)){
                board[row][col]=1;
                boolean next=check(board,row+1,size);
                if(next) 
                	return true;
                board[row][col]=0;
            }
        }
        return false;
    }
    public static boolean place(int board[][],int row,int col,int size){
        for(int x=0;x<row;x++)
            if(board[x][col]==1) 
            	return false;
        int x=row,y=col;
        while(x>=0 && y>=0)
            if(board[x--][y--]==1) 
            	return false;
        x=row;y=col;
        while(x>=0 && y<size)
            if(board[x--][y++]==1) 
            	return false;
        return true;
    }
    public static void display(int board[][],int size){
        for(int x=0;x<size;x++){
            for(int y=0;y<size;y++)
                System.out.print(board[x][y]+" ");
            System.out.println();
        }
    }
}
