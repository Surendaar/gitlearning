package com.level1;

import java.util.Scanner;

public class numberOfDays {
public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	int tc=sc.nextInt();
	while(tc>0) {
		int size = sc.nextInt();
		long mat[] = new long[size];
		for(int i=0; i<size; i++)
			mat[i]=sc.nextInt();
		long distance=sc.nextInt();
		System.out.println(whichDay(size, mat, distance));
		tc--;
	}
}

private static int whichDay(int size, long[] mat, long distance) {
	// TODO Auto-generated method stub
	long total=0;
	for(int i=0; i<size; i++) {
		total=total+mat[i];
		if(total>=distance)
		return i+1;
	}
	distance=distance%total;
	return whichDay(size, mat, distance);
}
}
