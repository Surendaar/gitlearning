package com.level1;

public class placingQueenUsingBackTracking {
	static int size=8;
	static int[][] mat = new int[8][8];
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int queen[] = {1, 2, 3, 4, 5, 6, 7, 8};
		display();
		placenqueens(queen);
	}

	private static void placenqueens(int[] queen) {
		// TODO Auto-generated method stub
		boolean check=false;
		if(check==true)
		{
			System.out.println("Final Queens places");
			display();
		}
		else
		for(int i=0; i<size; i++)
			for(int j=1; j<size; j++) {
				makeChessBoardAsZero(mat);
				check=startingPosition(i, j, queen);
			}
	}

	private static void makeChessBoardAsZero(int[][] mat2) {
		// TODO Auto-generated method stub
		for(int i=0; i<size; i++)
			for(int j=0; j<size; j++)
				mat[i][j]=0;
	}

	private static boolean startingPosition(int i, int j, int[] queen) {
		// TODO Auto-generated method stub
		int totalSize=size*size;
		int queensPlacedCount=0;
		
		if(queensPlacedCount==size)
			return true;
		else if(totalSize==0)
			return false;
		else {
			boolean attacked = checkIsAttacked(i, j);
			if(attacked==false) {
				totalSize=placeQueen(queen[queensPlacedCount], i, j, totalSize);
				queensPlacedCount++;
			}
		}
		return false;
	}

	private static int placeQueen(int q, int i, int j, int totalSize) {
		// TODO Auto-generated method stub
		mat[i][j]=q;
		totalSize=markattackedCells(i, j, q, totalSize);
		display();
		return totalSize;
	}

	private static int markattackedCells(int a, int b, int q, int totalSize) {
		// TODO Auto-generated method stub
		for(int i=0; i<size; i++)
			if(mat[i][b]==0) {
				mat[i][b]=q;
				totalSize--;
			}
		for(int j=0; j<size; j++)
			if(mat[a][j]==0) {
			mat[a][j]=q;
			totalSize--;
		}
		for(int i=a, j=b; i<size && j<size; i++, j++)
			if(mat[i][j]==0){
				mat[i][j]=q;
				totalSize--;
			}
		for(int i=0, j=0; i<a && j<b; i++, j++)
			if(mat[i][j]==0){
				mat[i][j]=q;
				totalSize--;
			}
		for(int i=a, j=b; i>0 && j>0; i--, j--)
			if(mat[i][j]==0){
				mat[i][j]=q;
				totalSize--;
			}
		for(int i=a, j=0; i>0 && j<b; i--, j++)
			if(mat[i][j]==0){
				mat[i][j]=q;
				totalSize--;
			}
		for(int i=0, j=b; i<a && j>0; i++, j--)
			if(mat[i][j]==0){
				mat[i][j]=q;
				totalSize--;
			}
		System.out.println("Total Size ="+totalSize);
		return totalSize;
	}

	private static boolean checkIsAttacked(int i, int j) {
		// TODO Auto-generated method stub
		if(mat[i][j]==0)
			return false;
		else 
			return true;
	}

	private static void display() {
		// TODO Auto-generated method stub
		for(int i=0; i<size; i++)
		{
			System.out.println();
			for(int j=0; j<size; j++)
				System.out.print(mat[i][j]+" ");
		}
	}

}
